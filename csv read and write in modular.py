from csv import DictReader,DictWriter                                          #imoprttion of csv file
with open("givenfile.csv",'r',newline='') as rf:                                          #openinig of givenfile
 with open("write_csv.csv",'w',newline='') as wf:                                         #opening of created new csv file
     csv_reader=DictReader(rf)                                                 #reading of csv file by dict reader and store in csv reader
     csv_writer=DictWriter(wf,fieldnames=['Year','Industry_aggregation_NZSIOC','Industry_name_NZSIOC','Variable_code','Variable_name']) #fieldnames which we write in created csv and store in csv writer
     csv_writer.writeheader()                                                  #writing of header
     for row in csv_reader:                                                     #looping for csv file reading
         Year,Industry_aggregation_NZSIOC,Industry_name_NZSIOC,Variable_code,Variable_name=row['Year'],row['Industry_aggregation_NZSIOC'],row['Industry_name_NZSIOC'],row['Variable_code'],row['Variable_name']#header name
         csv_writer.writerow({ #writing of field in new created csv file
         'Year':Year,
         'Industry_aggregation_NZSIOC':Industry_aggregation_NZSIOC,
         'Industry_name_NZSIOC':Industry_name_NZSIOC,
         'Variable_code':Variable_code,
         'Variable_name':Variable_name
         })
print('data inserted') #to check output of my excution
         
