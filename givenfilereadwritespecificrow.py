import csv
with open('givenfile.csv','r',newline='') as csv_file:
    csv_reader=csv.DictReader(csv_file)

    with open('new_givenfile.csv','w',newline='') as new_file:
        fieldnames=['Year','Industry_aggregation_NZSIOC','Industry_name_NZSIOC','Variable_code','Variable_name']

        csv_writer=csv.DictWriter(new_file,fieldnames=fieldnames,delimiter=',')

        csv_writer.writeheader()

        for line in csv_reader:
            del line['Industry_code_NZSIOC']
            del line['Units']
            del line['Variable_category']
            del line['Value']
            del line['Industry_code_ANZSIC06']
            csv_writer.writerow(line)
print("data insert")
